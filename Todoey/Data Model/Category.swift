//
//  Category.swift
//  Todoey
//
//  Created by Brandon Lambert on 4/29/19.
//  Copyright © 2019 Brandon Lambert. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name : String = ""
    let items = List<Item>()
}
